/*
File: ChicagoPizzaStore.java
Concrete Subclass of Pizza Store for factory pattern
Author: Jason Beere, Adnan Aziz, Wei-Lei Hung, & Head First Design Patterns
Java 7

Class: Dr. Adnan Aziz - Software Design Patterns
TA: Wei-Lun Hung
UT Austin - Clee
Summer 2013
Due: June 30, 2013
*/

package factorymethod;

public class ChicagoPizzaStore extends PizzaStore {
	
	Pizza createPizza(String item) {
		Pizza pizzaType = null;
		
		if (item.equals("cheese")) {
			pizzaType = new ChicagoStyleCheesePizza();
		} 
		else if (item.equals("clam")) {
			pizzaType =  new ChicagoStyleClamPizza();
		} 
		else if (item.equals("pepperoni")) {
			pizzaType =  new ChicagoStylePepperoniPizza();
		} 
		else if (item.equals("veggie")) {
			pizzaType =  new ChicagoStyleVeggiePizza();
		} 

		return pizzaType;
	}
 }
