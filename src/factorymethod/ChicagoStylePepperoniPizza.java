/*
File: ChicagoStylePepperoniPizza.java
Concrete Subclass of Pizza for factory pattern
Author: Jason Beere, Adnan Aziz, Wei-Lei Hung, & Head First Design Patterns
Java 7

Class: Dr. Adnan Aziz - Software Design Patterns
TA: Wei-Lun Hung
UT Austin - Clee
Summer 2013
Due: June 30, 2013
*/

package factorymethod;

public class ChicagoStylePepperoniPizza extends Pizza { // This is a type of product
	public ChicagoStylePepperoniPizza() {
		name = "Chicago Style Pepperoni Pizza";
		dough = "Extra Thick Crust Dough";
		sauce = "Plum Tomato Sauce";
 
		toppings.add("Shredded Mozzarella Cheese");
		toppings.add("Black Olives");
		toppings.add("Spinach");
		toppings.add("Eggplant");
		toppings.add("Sliced Pepperoni");
	}
 
	void cut() {
		System.out.println("Cutting the pizza into square slices");
	}
}
