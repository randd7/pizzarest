/*
File: NYStylePepperoniPizza.java
Concrete Subclass of Pizza for factory pattern
Author: Jason Beere, Adnan Aziz, Wei-Lei Hung, & Head First Design Patterns
Java 7

Class: Dr. Adnan Aziz - Software Design Patterns
TA: Wei-Lun Hung
UT Austin - Clee
Summer 2013
Due: June 30, 2013
*/

package factorymethod;

public class NYStylePepperoniPizza extends Pizza { // This is a type of product

	public NYStylePepperoniPizza() {
		name = "NY Style Pepperoni Pizza";
		dough = "Thin Crust Dough";
		sauce = "Marinara Sauce";
 
		toppings.add("Grated Reggiano Cheese");
		toppings.add("Sliced Pepperoni");
		toppings.add("Garlic");
		toppings.add("Onion");
		toppings.add("Mushrooms");
		toppings.add("Red Pepper");
	}
}
