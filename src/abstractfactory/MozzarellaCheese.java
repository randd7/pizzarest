/*
File: MozzarellaCheese.java
Concrete Implementation of Cheeses for abstract factory pattern
Author: Jason Beere, Adnan Aziz, Wei-Lei Hung, & Head First Design Patterns
Java 7

Class: Dr. Adnan Aziz - Software Design Patterns
TA: Wei-Lun Hung
UT Austin - Clee
Summer 2013
Due: June 30, 2013
*/

package abstractfactory;

public class MozzarellaCheese implements Cheese{

	public String toString() {
		return "Shredded Mozzarella";
	}
}
