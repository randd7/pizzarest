/*
File: PepperoniPizza.java
Concrete Subclass of Pizza for abstract factory pattern
Author: Jason Beere, Adnan Aziz, Wei-Lei Hung, & Head First Design Patterns
Java 7

Class: Dr. Adnan Aziz - Software Design Patterns
TA: Wei-Lun Hung
UT Austin - Clee
Summer 2013
Due: June 30, 2013
*/

package abstractfactory;

public class PepperoniPizza extends Pizza {
	PizzaIngredientFactory ingredientFactory;
 
	public PepperoniPizza(PizzaIngredientFactory ingredientFactory) {
		this.ingredientFactory = ingredientFactory;
	}
 
	void prepare() {
		System.out.printf ("Preparing %s\n", name);
		cheese = ingredientFactory.createCheese();
		dough = ingredientFactory.createDough();
		pepperoni = ingredientFactory.createPepperoni();
		sauce = ingredientFactory.createSauce();
		veggies = ingredientFactory.createVeggies();
	}
}
